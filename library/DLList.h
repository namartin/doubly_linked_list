#ifndef DLLIST_H
#define DLLIST_H

#include <stdio.h>
#include <stdlib.h>


class DLList 
{
public:
    struct node
    {
      int32_t data;
      node* ptr_next;
      node* ptr_prev;
      
      ~node();
    };
    
private:
    node* head;
    node* tail;
    int32_t size;

public:
    /// Constructor
    DLList();
    
    /// Copy constructor
    DLList(const DLList& orig);
    
    /// Destructor
    virtual ~DLList();
//    ~DLList();
    
    /// Append a node to the end of the list
    /// 
    /// \param value         Value of the node's data
    ///
    /// \return 0 if successful; else nonzero
    int32_t appendNode(int32_t value);
    
    /// Insert a node at the given index
    /// 
    /// \param index         Index where to insert the node
    /// \param value         Value for the node's data
    ///
    /// \return 0 if successful; else nonzero
    int32_t insertNode(int32_t index, int32_t value);
    
    /// Delete node at the index
    /// 
    /// \param index         Index of the node to be deleted
    ///
    /// \return 0 if successful; else nonzero
    int32_t deleteNode(int32_t index);
    
    /// Return the total number of nodes
    /// 
    /// \return 
    int32_t getSize() const;
    
    /// Print list to console
    void displayList() const;
    
    node* findHead() const;
    
    node* findTail() const;
    
    /// Get node at a given index
    /// 
    /// \param index         Index of the node you want
    ///
    /// \return a pointer to the node at the given index
    node* getNode(int32_t index) const;
};

#endif /* DLLIST_H */