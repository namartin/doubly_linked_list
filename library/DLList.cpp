#include "DLList.h"
#include <inttypes.h>
#include <iostream>

using namespace std;


DLList::DLList()
: head(0)
, tail(0)
, size(0)
{
}

DLList::DLList(const DLList& orig)
{
}

DLList::~DLList()
{
    // delete each node
    int32_t orig_size = size;
    for(int32_t i = 0; i < orig_size; i++)
    {
        deleteNode(0); // keep deleting the head
    }
}

DLList::node::~node()
{
}

int32_t DLList::appendNode(int32_t value)
{
    return insertNode(size, value); // insert node at the end
}

int32_t DLList::insertNode(int32_t index, int32_t value)
{
    if(index < 0 || index > size) // index out of bounds
    {
        return -1;
    }
    
    node* temp = new node;
    temp->data = value;
    temp->ptr_next = 0;
    temp->ptr_prev = 0;
    
    if(!size) // empty list -- the only index possible to get here is 0
    {
        head = temp;
        tail = temp;
    }
    else if(index == 0) // insert at beginning
    {
        temp->ptr_next = head;
        head->ptr_prev = temp;
        head = temp;
    }
    else if(index == size) // insert at end
    {
        temp->ptr_prev = tail; // attach this at the end
        tail->ptr_next = temp;
        tail = temp;      // move the tail
    }
    else // insert somewhere in the middle
    {
        node* prev_node = getNode(index - 1);
        node* next_node = getNode(index);
        
        temp->ptr_next = next_node;
        next_node->ptr_prev = temp;
        
        temp->ptr_prev = prev_node;
        prev_node->ptr_next = temp;
    }
    
    size++;
    
    return 0;
}

int32_t DLList::deleteNode(int32_t index)
{
    if(!size || index < 0 || index >= size) // empty list or index out of bounds
    {
        return -1;
    }
    
    node* node_to_delete = getNode(index);
    node* prev_node = 0;
    node* next_node = 0;
    
    if(size == 1)
    {
        head = 0;
        tail = 0;
        delete node_to_delete;
        size--;
        
        return 0;
    }
    
    // if made it this far then list has at least 2 nodes
    
    if(index > 0) // not first node -- get previous node
    {
        prev_node = getNode(index - 1);
    }
    if(index < (size - 1)); // not last node -- get next node
    {
        next_node = getNode(index + 1);
    }
    
    if(index == 0) // delete 1st node -- move head
    {
        next_node->ptr_prev = 0;
        head = next_node;
    }
    else if(index == (size - 1)) // delete last node -- move tail
    {
        prev_node->ptr_next = 0;
        tail = prev_node;
    }
    else // delete node somewhere in the middle
    {
        prev_node->ptr_next = next_node;
        next_node->ptr_prev = prev_node;
    }
        
    delete node_to_delete;
    size--;
    
    return 0;
}

int32_t DLList::getSize() const 
{
    return size;
}

// TODO: this is where the Valgrind error is.  EVERYTHING gives the error "Invalid read of size 4" 
//      (e.g. printing "Emtpy list", going through the for loop but everything inside commented out, etc.)
void DLList::displayList() const
{
    printf("\n"); // blank space before for readability
    
    if(!size)
    {
        printf("Empty list\n");
    }
    else
    {   
        node* curr_node;
        for(int32_t i = 0; i < size; i++)
        {
            if(i == 0)
            {
                curr_node = head;
            }
            else
            {
                curr_node = curr_node->ptr_next;
            }
            
            printf("%i\t", (int)(curr_node->data));
        }
    }
    
    printf("\n"); // blank space after for readability
}

DLList::node* DLList::getNode(int32_t index) const
{
    if(index < 0 || index >= size) // index out of bounds
    {
        return NULL;
    }
    
//    if(!head) // empty list -- unreachable since its size == 0 so it will always go into the if statement above
//    {
//        return NULL;
//    }
    
    node* curr_node;
    for(int32_t i = 0; i <= index; i++)
    {
        if(i == 0)
        {
            curr_node = head;
        }
        else
        {
            curr_node = curr_node->ptr_next;
        }
    }
    
    return curr_node;
}

DLList::node* DLList::findHead() const
{
    return head;
}

DLList::node* DLList::findTail() const
{
    return tail;
}